//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//Name- main.js
//Description:In this file we get input fiels from hotel.html file and read the files such as csv , date, time. the csv file contains the information about hotel opening days and time  
//Input- csv , date, time
//Output- we will get the information of hotel at the  given input time and days
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function file_upload() {
	// csv file input form user
    var file = document.getElementById("upload_file") 
    //get input date from usser 
    var date = document.getElementById("date").value;
    // get input time from user	
	var time = document.getElementById("time").value;	
    var d = new Date(date + " " + time);
    var days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat","sun"];
    var time_str = time.toString();
    var hour = time_str.substring(0, 2);
    var min = time_str.substring(3);
    var in_time = (parseInt(hour) * 60) + (parseInt(min));
    var index;
    var Am = "am";
    var Pm = "pm"
    var in_day = d.getDay();
    	in_day=in_day-1;
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
    // read the csv file form input by using reader function 		
    if (regex.test(file.value.toLowerCase())) {			
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
            	//split the content fo csv file by \n and stored in a row array
                var rows = e.target.result.split("\n"); 
			    for (var i = 0; i < rows.length; i++) {
                    var str = "";
                    var cells = rows[i].split(",");
                    var hotel=cells[0];
                    for (var j = 1; j < cells.length; j++) {
                        str += cells[j];
                    }
                    var n = str.toString();
                    n = n.replace(/"/g, "");
                    //split the each row by /
                    var m = n.split("/");			
                    for (var p = 0; p < m.length; p++) {
                    	var low_day = -1;
                        var high_day = -1;
                        // match the reg of day format
                        var reg = /(\w{3}-\w{3})/i;
                        if (reg.test(m[p])) {					
                            var day_limit = m[p].match(reg);
                            var days_separated = day_limit[0].split('-');
                             var low_index = days.indexOf(days_separated[0]);
                            var up_index = days.indexOf(days_separated[1]);
                            if(low_index==-1)
                            	low_index=6;
                            if(up_index==-1)
                            	up_index=6;
                        }
                         var str1 = m[p].replace(/-/g, " ")
                         // match the time format
                         var time_arr = str1.match(/[0-9]+:*[0-9]* am|[0-9]+:*[0-9]* pm/g); 	
                         var check1 = time_arr[0].includes("pm");
						 var check2 = time_arr[1].includes("pm");
						 // time covert part start
                            if (check1) {
                                var start_time = time_arr[0].match(/[0-9]+[0-9]*/g);
                                if (parseInt(start_time[0]) != 12) {
                                    if (start_time.length == 2) {
                                        var out_start_time = (((parseInt(start_time[0])) + 12) * 60) + (parseInt(start_time[1]))
                                    } else {
                                        var out_start_time = (((parseInt(start_time[0])) + 12) * 60)
                                    }
                                }
                                 else if (parseInt(start_time[0]) == 12) {
                                    if (start_time.length == 2) {
                                        var out_start_time = ((parseInt(start_time[0])) * 60) + (parseInt(start_time[1]))
                                    } else {
                                        var out_start_time = ((parseInt(start_time[0])) * 60)
                                    }
                                }	
                            }
                           	else{
                           		var start_time = time_arr[0].match(/[0-9]+[0-9]*/g);
                           		if (parseInt(start_time[0]) != 12) {
                                    if (start_time.length == 2) {
                                        var out_start_time = ((parseInt(start_time[0]))* 60) + (parseInt(start_time[1]))
                                    } else {
                                        var out_start_time = ((parseInt(start_time[0]))*60)
                                    }
                                }
                                else if (parseInt(start_time[0]) == 12) {
                                    if (start_time.length == 2) {
                                        var out_start_time = ((parseInt(start_time[0])) * 60) + (parseInt(start_time[1]))
                                    } else {
                                        var out_start_time = ((parseInt(start_time[0])) * 60)
                                    }
                                }
                           	}
                           	if (check2) {
                           		var end_time = time_arr[1].match(/[0-9]+[0-9]*/g);

                                if (parseInt(end_time[0]) != 12) {
                                    if (end_time.length == 2) {
                                        var out_end_time = (((parseInt(end_time[0])) + 12) * 60) + (parseInt(end_time[1]))
                                    } else {
                                        var out_end_time = (((parseInt(end_time[0])) + 12) * 60)
                                    }

                                } else if (parseInt(end_time[0]) == 12) {
                                    if (end_time.length == 2) {
                                        var out_end_time = (((parseInt(end_time[0]))+12)*60) + (parseInt(end_time[1]))
                                    } else {
                                        var out_end_time = (((parseInt(end_time[0]))+12)*60)
                                    }
                                }
                           	}
                           	else{
                           		var end_time = time_arr[1].match(/[0-9]+[0-9]*/g);
                                if (parseInt(end_time[0]) != 12) {
                                    if (end_time.length == 2) {
                                        var out_end_time = ((parseInt(end_time[0])) * 60) + (parseInt(end_time[1]))
                                    } else {
                                        var out_end_time = ((parseInt(end_time[0]))* 60)
                                    }

                                } else if (parseInt(end_time[0]) == 12) {
                                    if (end_time.length == 2) {
                                        var out_end_time = ((parseInt(end_time[0])) * 60) + (parseInt(end_time[1]))
                                    } else {
                                        var out_end_time = ((parseInt(end_time[0])) * 60)
                                    }
                                }
                           	}
                           		//time format  convert end
                           		//print the  hotels
                           		if(in_day>=low_index && in_day<=up_index){
                           		if(in_time>=out_start_time && in_time<=out_end_time){
                           			document.write("<strong>"+hotel+"</strong><br>"+"open time-"+time_arr[0]+" close time-"+time_arr[1]+"<br>");
                           		}
                             break;
                         }
                    }
                }
                
            }
            reader.readAsText(file.files[0]);    
        }
    }
}







